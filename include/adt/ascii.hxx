#ifndef ADT__ASCII_HXX
#define ADT__ASCII_HXX

#include <nxx/enum.hxx>

namespace ascii {
	enum class control_code : char {
		NUL,           // Null
		SOH,           // Start of Heading
		STX,           // Start of Text
		ETX,           // End of Text
		EOT,           // End of Transmission
		ENQ,           // Enquiry
		ACK,           // Acknoledge
		BEL,           // Bell, Alert
		BS,            // Backspace
		HT,            // Horizοntal Tabulation
		LF,            // Line Feed
		VT,            // Virtical Tabulation
		FF,            // Form Feed
		CR,            // Carage Return
		SO,            // Shift Out
		SI,            // Shift In
		DLE,           // Data Link Escape
		DC1,           // Device Control One
		DC2,           // Device Control Two
		DC3,           // Device Control Three
		DC4,           // Device Control Four
		NAK,           // Negative Acknoledgement
		SYN,           // Synchronus Idle
		ETB,           // End of Transition Block
		CAN,           // Cancel
		EM,            // End of Medium
		SUB,           // Substitute
		ESC,           // Escape
		FS,            // File Separator
		GS,            // Group Separator
		RS,            // Record Separator
		US,            // Unit Separator
		SP,            // Space
		DEL = '\x7f',  // Delete
	};
}  // namespace ascii

NXX_ENABLE_UNDERLYING_ENUM(ascii::control_code);

#endif  // ifndef ADT__ASCII_HXX
