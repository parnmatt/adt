#ifndef ADT__ADT_HXX
#define ADT__ADT_HXX

#include <iosfwd>
#include <optional>
#include <string>
#include <vector>

#include <nxx/strong_typedef.hxx>

#include "ascii.hxx"

namespace adt {

	namespace detail {

		template <typename T>
		struct level : nxx::strong_typedef<level<T>, T> {
			using nxx::strong_typedef<level, T>::strong_typedef;
			using s = std::vector<level>;
		};

		template <typename T>
		auto operator<<(std::ostream&, level<T> const&) -> std::ostream&;

	}  // namespace detail

	using unit = detail::level<std::string>;
	using record = detail::level<unit::s>;
	using group = detail::level<record::s>;
	using header = detail::level<group::s>;

	struct file {
		using s = std::vector<file>;
		void clear() noexcept;
		std::optional<adt::header> header{};
		group::s groups{};
	};

	auto operator<<(std::ostream&, file const&) -> std::ostream&;


	class specification {
	private:
		template <typename T>
		class level : public T {
		public:
			using value_type = T;

			auto spec() const -> specification const& { return m_spec; }

		private:
			friend specification;

			explicit level(specification const& spec)
			    : m_spec{spec} {}

			explicit level(specification const& spec, value_type const& value)
			    : value_type{value}
			    , m_spec{spec} {}

			specification const& m_spec;
		};

		template <typename T>
		class make {
		public:
			constexpr make(specification const& spec)
			    : m_spec{spec} {}

			auto constexpr operator()() const { return T{m_spec}; }
			auto constexpr operator()(typename T::value_type const& value) const { return T{m_spec, value}; }

		private:
			specification const& m_spec;
		};

	public:
		using unit = level<adt::unit>;
		using record = level<adt::record>;
		using group = level<adt::group>;
		using header = level<adt::header>;
		using file = level<adt::file>;

		constexpr specification() = default;
		explicit constexpr specification(char start_of_header, char start_of_text, char end_of_text,
		                                 char unit_separator, char record_separator, char group_separator,
		                                 char file_separator)
		    : start_of_header{start_of_header}
		    , start_of_text{start_of_text}
		    , end_of_text{end_of_text}
		    , unit_separator{unit_separator}
		    , record_separator{record_separator}
		    , group_separator{group_separator}
		    , file_separator{file_separator} {}

		auto parse(std::istream& is) const -> adt::file::s;
		auto whitespace(std::istream& is) const -> std::istream&;

		make<unit> const make_unit{*this};
		make<record> const make_record{*this};
		make<group> const make_group{*this};
		make<header> const make_header{*this};
		make<file> const make_file{*this};

		char start_of_header{*ascii::control_code::SOH};
		char start_of_text{*ascii::control_code::STX};
		char end_of_text{*ascii::control_code::ETX};
		char unit_separator{*ascii::control_code::US};
		char record_separator{*ascii::control_code::RS};
		char group_separator{*ascii::control_code::GS};
		char file_separator{*ascii::control_code::FS};

		bool skip_whitespace{false};
	};

	template <typename T>
	auto operator<<(std::ostream&, specification::level<T> const&) -> std::ostream&;

	template <typename T>
	auto operator>>(std::istream&, specification::level<T>&) -> std::istream&;


	auto constexpr inline adt = specification{};

}  // namespace adt

#endif  // ifndef ADT__ADT_HXX
