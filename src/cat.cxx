#include <fstream>
#include <iostream>

#include <adt/adt.hxx>

auto main(int argc, char* argv[]) -> int {
	auto constexpr& spec = adt::adt;

	bool first{true};
	for (int i{1}; i < argc; ++i) {
		auto file = std::fstream{argv[i]};
		if (file.is_open()) {
			if (!first)
				std::cout << spec.file_separator;
			first = false;
			std::cout << file.rdbuf();
		}
	}
}
