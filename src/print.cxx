#include <iostream>

#include <adt/adt.hxx>

auto main() -> int {
	auto constexpr& spec = adt::adt;
	auto const files = spec.parse(std::cin);

	for (auto const& file : files)
		std::cout << file;
}
