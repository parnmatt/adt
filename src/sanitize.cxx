#include <algorithm>
#include <iostream>
#include <iterator>

#include <nxx/iterator.hxx>

#include <adt/adt.hxx>

auto main() -> int {
	auto constexpr& spec = adt::adt;
	auto const files = spec.parse(std::cin);

	std::copy(std::begin(files), std::end(files),
	          nxx::iterator::transform_output{
	              nxx::iterator::infix_ostream<adt::specification::file>{std::cout, spec.file_separator},
	              [&](auto const& file) { return spec.make_file(file); }});
}
