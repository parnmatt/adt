#include <algorithm>
#include <ios>
#include <iostream>
#include <iterator>
#include <sstream>

#include <nxx/algorithm.hxx>
#include <nxx/iterator.hxx>

#include <adt/adt.hxx>

namespace {
	class indent {
	public:
		explicit indent(long level)
		    : m_level{level} {}

		auto level() const { return m_level; }

		int const static index;

	private:
		friend auto operator<<(std::ostream&, indent&) -> std::ostream&;

		long m_level{};
	};

	int const indent::index = std::ios_base::xalloc();

	auto operator<<(std::ostream& os, indent const& indent) -> std::ostream& {
		nxx::repeat_n(indent.level(), [&] { os << '\t'; });
		return os;
	}


	class indent_guard {
	public:
		explicit indent_guard(std::ostream& os)
		    : m_os{&os} {
			++m_os->iword(indent::index);
		}

		~indent_guard() { --m_os->iword(indent::index); }

	private:
		std::ostream* m_os{};
	};


}  // namespace


namespace adt {

	// unit
	auto operator<<(std::ostream& os, unit const& unit) -> std::ostream& {
		return os << indent{os.iword(indent::index)} << "unit: " << *unit << '\n';
	}

	template <>
	auto operator<<(std::ostream& os, specification::unit const& unit) -> std::ostream& {
		return os << *unit;
	}

	template <>
	auto operator>>(std::istream& is, specification::unit& unit) -> std::istream& {
		unit->clear();
		auto const& spec = unit.spec();

		spec.whitespace(is);
		std::ostringstream ss{};
		for (char c{}; is >> c;) {
			if (c == spec.unit_separator) {
				break;
			} else if (c == spec.record_separator || c == spec.group_separator || c == spec.start_of_text
			           || c == spec.end_of_text || c == spec.file_separator) {
				is.unget();
				break;
			} else {
				ss << c;
			}
		}

		*unit = ss.str();
		return is;
	}


	// record
	auto operator<<(std::ostream& os, record const& record) -> std::ostream& {
		os << indent{os.iword(indent::index)} << "record:\n";
		indent_guard g{os};

		for (auto const& unit : *record)
			os << unit;

		return os;
	}

	template <>
	auto operator<<(std::ostream& os, specification::record const& record) -> std::ostream& {
		auto const& spec = record.spec();

		std::copy(std::begin(*record), std::end(*record),
		          nxx::iterator::transform_output{
		              nxx::iterator::infix_ostream<specification::unit>{os, spec.unit_separator},
		              [&](auto const& unit) { return spec.make_unit(unit); }});

		return os;
	}

	template <>
	auto operator>>(std::istream& is, specification::record& record) -> std::istream& {
		record->clear();
		auto const& spec = record.spec();

		spec.whitespace(is);
		for (auto unit = spec.make_unit(); is;) {
			is >> unit;
			record->push_back(unit);

			if (char c{}; (is >> c, c == spec.record_separator)) {
				break;
			} else if (c == spec.group_separator || c == spec.start_of_text || c == spec.end_of_text
			           || c == spec.file_separator) {
				is.unget();
				break;
			} else {
				is.unget();
			}
		}

		return is;
	}


	// group
	auto operator<<(std::ostream& os, group const& group) -> std::ostream& {
		os << indent{os.iword(indent::index)} << "group:\n";
		indent_guard g{os};

		for (auto const& record : *group)
			os << record;

		return os;
	}

	template <>
	auto operator<<(std::ostream& os, specification::group const& group) -> std::ostream& {
		auto const& spec = group.spec();
		std::copy(std::begin(*group), std::end(*group),
		          nxx::iterator::transform_output{
		              nxx::iterator::infix_ostream<specification::record>{os, spec.record_separator},
		              [&](auto const& record) { return spec.make_record(record); }});
		return os;
	}

	template <>
	auto operator>>(std::istream& is, specification::group& group) -> std::istream& {
		group->clear();
		auto const& spec = group.spec();

		spec.whitespace(is);
		for (auto record = spec.make_record(); is;) {
			is >> record;
			group->push_back(record);

			if (char c{}; (is >> c, c == spec.group_separator)) {
				break;
			} else if (c == spec.start_of_text || c == spec.end_of_text || c == spec.file_separator) {
				is.unget();
				break;
			} else {
				is.unget();
			}
		}

		return is;
	}


	// header
	auto operator<<(std::ostream& os, header const& header) -> std::ostream& {
		os << indent{os.iword(indent::index)} << "header:\n";
		indent_guard g{os};

		for (auto const& group : *header)
			os << group;

		return os;
	}

	template <>
	auto operator<<(std::ostream& os, specification::header const& header) -> std::ostream& {
		auto const& spec = header.spec();
		os << spec.start_of_header;

		std::copy(std::begin(*header), std::end(*header),
		          nxx::iterator::transform_output{
		              nxx::iterator::infix_ostream<specification::group>{os, spec.group_separator},
		              [&](auto const& group) { return spec.make_group(group); }});

		return os << spec.start_of_text;
	}

	template <>
	auto operator>>(std::istream& is, specification::header& header) -> std::istream& {
		header->clear();
		auto const& spec = header.spec();

		spec.whitespace(is);
		if (char c{}; (is >> c, c != spec.start_of_header)) {
			is.unget();
			is.clear(std::ios_base::failbit);
			return is;
		}

		spec.whitespace(is);
		for (auto group = spec.make_group(); is;) {
			is >> group;
			header->push_back(group);

			if (char c{}; (is >> c, c == spec.start_of_text)) {
				break;
			} else if (c == spec.end_of_text || c == spec.file_separator) {
				is.unget();
				break;
			} else {
				is.unget();
			}
		}

		return is;
	}


	// file
	void file::clear() noexcept {
		header = {};
		groups.clear();
	}

	auto operator<<(std::ostream& os, file const& file) -> std::ostream& {
		os << indent{os.iword(indent::index)} << "file:\n";
		indent_guard g{os};

		if (file.header)
			os << *file.header;

		for (auto const& group : file.groups)
			os << group;

		return os;
	}

	template <>
	auto operator<<(std::ostream& os, specification::file const& file) -> std::ostream& {
		auto const& spec = file.spec();

		if (file.header)
			os << spec.make_header(*file.header);

		std::copy(std::begin(file.groups), std::end(file.groups),
		          nxx::iterator::transform_output{
		              nxx::iterator::infix_ostream<specification::group>{os, spec.group_separator},
		              [&](auto const& group) { return spec.make_group(group); }});

		if (file.header)
			os << spec.end_of_text;

		return os;
	}

	template <>
	auto operator>>(std::istream& is, specification::file& file) -> std::istream& {
		file.clear();
		auto const& spec = file.spec();

		spec.whitespace(is);
		if (is.peek() == spec.start_of_header) {
			auto header = spec.make_header();
			is >> header;
			file.header = header;
		}

		spec.whitespace(is);
		if (char c{}; (is >> c, c != spec.start_of_text))
			is.unget();

		spec.whitespace(is);
		for (auto group = spec.make_group(); is;) {
			is >> group;
			file.groups.push_back(group);

			if (char c{}; is >> c) {
				if (c == spec.end_of_text) {
					is >> c;
					if (c != spec.file_separator)
						is.unget();
					break;
				} else if (c == spec.file_separator) {
					is >> c;
					if (c != spec.start_of_text)
						is.unget();
					break;
				} else {
					is.unget();
				}
			}
		}

		return is;
	}


	auto specification::parse(std::istream& is) const -> adt::file::s {
		auto files = adt::file::s{};
		for (auto file = make_file(); is;) {
			std::cin >> file;
			files.push_back(file);
		}
		return files;
	}

	auto specification::whitespace(std::istream& is) const -> std::istream& {
		if (char c{}; skip_whitespace) {
			is >> std::skipws >> c;
			is.unget();
		}

		return is >> std::noskipws;
	}

}  // namespace adt
